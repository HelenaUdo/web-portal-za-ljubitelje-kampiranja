<?php
session_start();
include 'base.php'; 

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_SESSION['email'])) {
        $email = $_SESSION['email'];
        $ocjena = $_POST['rating'];
        $idDestinacije = $_POST['destinationId'];

        $existingRating = "SELECT * FROM ocjene WHERE idDestinacije = '$idDestinacije' AND email = '$email'";
        $result = $conn->query($existingRating);

        if ($result->num_rows > 0) {
            echo 'Već ste ocijenili ovu destinaciju.';
        } else {
            $sql = "INSERT INTO ocjene (idDestinacije, email, ocjena) VALUES ('$idDestinacije', '$email', '$ocjena')";
            if ($conn->query($sql) === TRUE) {
                echo 'Ocjena je uspješno spremljena.';
            } else {
                echo 'Greška prilikom spremanja ocjene: ' . $conn->error;
            }
        }
    } else {
        echo 'Niste prijavljeni. Molimo prijavite se kako biste ocijenili destinaciju.';
    }
}
?>