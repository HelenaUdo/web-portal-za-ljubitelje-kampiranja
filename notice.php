<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>WebCampAdmin Info</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php
            include "header.php";
            ?>
            <?php
            if (isset($_SESSION['resetMessage'])) {
                echo '<div class="alert-info">';
                echo '<h4>Obavijest</h4>';
                echo '<p>Korisnik s adresom <span class="email">' . $_SESSION['resetMessage'] . '</span> traži novu lozinku.</p>';
                echo '</div>';
                unset($_SESSION['resetMessage']);
            }
            
            $query = "SELECT * FROM info";
            $result = mysqli_query($conn, $query);

            if (mysqli_num_rows($result) > 0) {
                echo '<div class="alert-info">';
                echo '<h4>Obavijesti</h4>';
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '<p>Poruka: <span class="email">' . $row['email'] . '</span> traži novu lozinku.</p>';
                }
                echo '</div>';
            }
            ?>
        </div>
    </body>
</html>