<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Unos destinacije</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php
            include "header.php";
            ?>

            <?php 
            require_once "base.php";

            if(isset($_POST['naziv'], $_POST['cijenaEuri'], $_POST['cijenaKune'], $_POST['opis'], $_POST['podrucje'], $_POST['kolicina'], $_POST['datum'])){
                unset($error);
  
                $naziv = $_POST['naziv'];
                $cijenaEuri = $_POST['cijenaEuri'];
                $cijenaKune = $_POST['cijenaKune'];
                $opis = $_POST['opis'];
                $podrucje = $_POST['podrucje'];
                $kolicina = $_POST['kolicina'];
                $datum = $_POST['datum'];
                $targetDir = "images/camp_img/";
                $slikaPrva = $targetDir . basename($_FILES["slikaPrva"]["name"]);
                $slikaDruga = $targetDir . basename($_FILES["slikaDruga"]["name"]);
                                
                if (move_uploaded_file($_FILES["slikaPrva"]["tmp_name"], $slikaPrva) && move_uploaded_file($_FILES["slikaDruga"]["tmp_name"], $slikaDruga)) {
                    $naziv = mysqli_real_escape_string($conn, $naziv);
                    $cijenaEuri = mysqli_real_escape_string($conn, $cijenaEuri);
                    $cijenaKune = mysqli_real_escape_string($conn, $cijenaKune);
                    $opis = mysqli_real_escape_string($conn, $opis);
                    $podrucje = mysqli_real_escape_string($conn, $podrucje);
                    $kolicina = mysqli_real_escape_string($conn, $kolicina);
                    $datum = mysqli_real_escape_string($conn, $datum);

                    $sql = "INSERT INTO destinacije (naziv, cijenaEuri, cijenaKune, opis, podrucje, kolicina, datum, slikaPrva, slikaDruga) 
                            VALUES ('$naziv', '$cijenaEuri', '$cijenaKune', '$opis', '$podrucje', '$kolicina', '$datum', '$slikaPrva', '$slikaDruga')";
                    if ($conn->query($sql)){
                        $_SESSION['success_message'] = "Vaši su podaci uspješno poslani";
                        header("location: destination_list.php");
                        exit();
                    }
                    else {
                        echo "Error: " . $sql . ": -" . mysqli_error($conn);
                    }
                } else {
                    echo "Error uploading images.";
                }
                mysqli_close($conn);
            }
            ?>

            <section id="destinacija_opis" class="section-p1">
                <div class="prviopis">
                    <h2>Unesite novu destinaciju:</h2>
                </div>
            </section>

            <section id="destinacija_unos" class="section-p1">
                <?php
                if (isset($_SESSION['success_message'])) {
                    echo "<h3>" . $_SESSION['success_message'] . "</h3>";
                    unset($_SESSION['success_message']);
                }
                ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <span>UNESITE PODATKE</span>
                    <input type="text" name="naziv" id="naziv" placeholder="Naziv destinacije" autocomplete="off">
                    <input type="text" name="cijenaEuri" id="cijenaEuri" placeholder="Cijena u eurima" autocomplete="off">
                    <input type="text" name="cijenaKune" id="cijenaKune" placeholder="Cijena u kunama" autocomplete="off">
                    <textarea name="opis" id="opis" placeholder="Opis" autocomplete="off"></textarea>
                    <input type="text" name="podrucje" id="podrucje" placeholder="Područje" autocomplete="off">
                    <input type="text" name="kolicina" id="kolicina" placeholder="Količina mjesta" autocomplete="off">
                    <input type="text" name="datum" id="datum" placeholder="Datum" autocomplete="off">
                    <label for="slikaPrva">Prva slika:</label>
                    <input type="file" name="slikaPrva" id="slikaPrva">  
                    <label for="slikaDruga">Druga slika:</label>
                    <input type="file" name="slikaDruga" id="slikaDruga">
                    <button class="send_normal" id="otkupise">Dodaj</button><br><br>
                </form>
            </section>

            <section id="destinacija_opis" class="section-p1">
                <div class="prviopis">
                    <h2>Popis destinacija:</h2>
                </div>
            </section>

            <?php
            // ispis
            $sql = "SELECT * FROM destinacije";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                echo "<table><tr><th> KAMP </th><th> SLIKA </th><th> OPIS </th><th> CIJENA U EURIMA </th><th> CIJENA U KUNAMA </th><th> PODRUČJE </th><th> MJESTA </th><th> DATUM </th></tr>";            
                
                while ($row = $result->fetch_assoc()) {
                    echo  "<tr> ";
                    echo "<td>" .$row['naziv']. "</td>
                        <td><img src='" .$row['slikaPrva']. "' alt='" .$row['naziv']. "' width='150'> <img src='" .$row['slikaDruga']. "' alt='" .$row['naziv']. "' width='150'></td>
                        <td class='opis-column'>" .$row['opis']. "</td>
                        <td>" .$row['cijenaEuri']. " €</td>
                        <td>" .$row['cijenaKune']. " kn</td>
                        <td>" .$row['podrucje']. "</td>
                        <td>" .$row['kolicina']. "</td>
                        <td>" .$row['datum']. "</td>
                        <td style='text-align: center; vertical-align: middle;'>
                            <a href='delete_camp.php?delete_id=".$row['idDestinacije']."'><i class='fa fa-trash'></i></a>
                            <a style='margin-left: 5px;' href='edit_destination.php?edit_id=".$row['idDestinacije']."'><i class='fa fa-edit'></i></a>
                        </td>";
                    echo  "</tr> ";
                }
                echo "</table>";
            } else {
                echo "0 results";
            }

            $conn->close();
            ?>
        </div>
    </body>
</html>
