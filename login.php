<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Prijava</title>
        <link rel="stylesheet" href="css/goin.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div>
            <?php 
            include "base.php";
            $msg='';
            if (isset($_POST['prijavise']) && !empty($_POST['email']) && !empty($_POST['lozinka'])){
                $email = $_POST['email'];
                $lozinka = $_POST['lozinka'];
        
                $sql = "SELECT * FROM korisnici WHERE email = '$email'";
                $result = mysqli_query($conn, $sql);
        
                if ($result->num_rows > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $hashedPassword = $row['lozinka'];
        
                    if (password_verify($lozinka, $hashedPassword)) {
                        $_SESSION['prijavljen'] = true;
                        $_SESSION['timeout'] = time();
                        $_SESSION['email'] = $email;
                        $_SESSION['ime'] = $row['ime'];
                        $_SESSION['prezime'] = $row['prezime'];
                        $_SESSION['uloga'] = 'kupac';
        
                        header("Location: front_page.php");
                        exit();
                    } else {
                        $msg = 'Krivo korisničko ime ili lozinka!';
                    }
                } else {
                    $msg = 'Korisnik s unesenim emailom nije pronađen!';
                }
            }

            if (isset($_POST['sendResetEmail']) && !empty($_POST['forgotEmail'])) {
                $forgotEmail = $_POST['forgotEmail'];
            
                $insertQuery = "INSERT INTO info (email) VALUES ('$forgotEmail')";
                mysqli_query($conn, $insertQuery);
            
                header("Location: login.php");
                exit();
            }

            ?>
        </div>
        <form action="" method="post">
            <div id="log" class="contain">
              <img src="images_base/logokampa-removebg-preview.png" alt="LogoKampa"><br>  
              <label for="email"><b>E-mail</b></label>
              <input type="text" placeholder="Unesite e-mail" name="email" required>
    
              <label for="lozinka"><b>Lozinka</b></label>
              <input type="password" placeholder="Unesite lozinku" name="lozinka" required>
          
              <button type="submit" class="goinbutton" name="prijavise">Prijava</button>

              <h4><?php echo $msg; ?></h4>
            </div>
            <div class="container login">
                <p class="zabb_loz"><a id="forgotPasswordLink" href="#">Zaboravio/la sam lozinku</a>.</p>
                <p>Nemate svoj račun? <a href="register.php">Registriraj</a>.</p>
                <p>Jeste li administrator? <a href="admin_log.php">Prijavi se kao admin</a>.</p>
            </div>
        </form>

         <!-- Modal za zaboravljenu lozinku -->
        <div id="forgotPasswordModal" class="modal_log">
            <div class="modal_log_content">
                <span class="close">&times;</span>
                <form action="" method="post">
                    <p>Unesite svoju e-mail adresu.</p>
                    <input type="email" id="forgotEmail" class="form-control" placeholder="Vaša e-mail adresa" name="forgotEmail" required>
                    <button type="submit" class="send_email_adm" name="sendResetEmail">Pošalji</button>
                </form>
            </div>
        </div>

        <script>
            var modal = document.getElementById('forgotPasswordModal');

            var forgotPasswordLink = document.getElementById('forgotPasswordLink');

            var closeBtn = document.getElementsByClassName('close')[0];

            forgotPasswordLink.onclick = function() {
                modal.style.display = 'block';
            }

            closeBtn.onclick = function() {
                modal.style.display = 'none';
            }

            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = 'none';
                }
            }
        </script>
    </body>
</html>
