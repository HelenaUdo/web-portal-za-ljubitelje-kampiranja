<?php
session_start();
include "base.php";

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['productId'])) {
    if (isset($_SESSION['email'])) {
        $userEmail = $_SESSION['email'];
        $productId = $_POST['productId'];

        $checkSql = "SELECT * FROM kosarica WHERE email = '$userEmail' AND IdProizvoda = $productId";
        $checkResult = mysqli_query($conn, $checkSql);

        if ($checkResult && mysqli_num_rows($checkResult) === 0) {
            // Ako proizvod nije u košarici, dodajte ga
            $insertSql = "INSERT INTO kosarica (IdProizvoda, email) VALUES ($productId, '$userEmail')";
            mysqli_query($conn, $insertSql);

            header("Location: product_details.php?id=$productId");
            exit();
        } else {
            echo "Proizvod je već dodan u vašu košaricu.";
        }
    } else {
        echo "Morate se prijaviti kako biste dodali proizvod u košaricu.";
    }
} else {
    echo "Neispravan zahtjev.";
}
?>