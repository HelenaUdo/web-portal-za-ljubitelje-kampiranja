<?php
session_start();
include "base.php";

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (isset($_SESSION['email'])) {
        $userEmail = $_SESSION['email'];
        $adresa = $_POST['adresa'];
        $brojMobitela = $_POST['brojMobitela'];
        $iban = $_POST['iban'];

        $sqlInsert = "INSERT INTO kupljeniproizvodi (email, IdProizvoda, adresa, brojMobitela, iban) 
                      SELECT '$userEmail', IdProizvoda, '$adresa', '$brojMobitela', '$iban' 
                      FROM kosarica 
                      WHERE email = '$userEmail'";

        if (mysqli_query($conn, $sqlInsert)) {
            $updateQuantities = "UPDATE proizvodi
                                SET kolicinaProizvoda = kolicinaProizvoda - 1
                                WHERE IdProizvoda IN (SELECT IdProizvoda FROM kosarica WHERE email = '$userEmail')";

            $sqlDelete = "DELETE FROM kosarica WHERE email = '$userEmail'";
            mysqli_query($conn, $sqlDelete);

            echo "success";
        } else {
            echo "error";
        }
    } else {
        echo "not_logged_in";
    }
}
?>