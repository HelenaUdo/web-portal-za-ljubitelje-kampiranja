<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>WebCampAdmin</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
          <?php
          include "header.php";
          ?>

          <section id="head">
              <div id="navbar">
                  <a class="active" href="admin_page.php">Kamping u Hrvatskoj</a>
              </div>
          </section>

          <section id="mjestaOdabira" class="section-p1">
                <div class="pro-container">
                    <a href="destination_list.php">Dodaj destinaciju</a>
                    <a href="products_list.php">Dodaj proizvod</a>
                    <a href="articles_list.php">Dodaj članak</a>
                </div>
          </section>

          <section id="graf" class="section-p1">
                <div id="chart_div"></div>
                <div id="product_chart_div"></div>
          </section>

        </div>

        <script src="js/chart_reservation.js"></script>
        <script src="js/chart_products.js"></script>
    </body>
</html>