<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Cart</title>
    <link rel="stylesheet" href="css/style_camp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
</head>
<body>
    <div class="content-container">
        <?php include "header.php"; ?>
        
        <section id="myCart" class="section-p1">
            <div class="header_container">
                <div class="back-button">
                    <a href="javascript:history.back()" class="butn btn-secondary">&lt;</a>
                </div>
                <h2>Moja košarica</h2>
            </div>

            <div class="cart-items-container">
                <?php
                if (isset($_SESSION['email'])) {
                    $userEmail = $_SESSION['email'];

                    if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["delete_product_from_cart"])) {
                        $cartItemIdToDelete = $_GET["delete_product_from_cart"];
                        $deleteSql = "DELETE FROM kosarica WHERE idKosarice = '$cartItemIdToDelete' AND email = '$userEmail'";
                        mysqli_query($conn, $deleteSql);

                        header("Location: cart.php");
                        exit();
                    }

                    $sql = "SELECT kosarica.*, proizvodi.naziv, proizvodi.slikaProizvoda, proizvodi.boja, proizvodi.cijenaEuri, proizvodi.cijenaKune
                            FROM kosarica
                            JOIN proizvodi ON kosarica.IdProizvoda = proizvodi.IdProizvoda
                            WHERE kosarica.email = '$userEmail'";

                    $result = mysqli_query($conn, $sql);

                    if ($result && mysqli_num_rows($result) > 0) {

                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<div class="cart-item">';
                                echo '<img src="' . $row['slikaProizvoda'] . '" alt="' . $row['naziv'] . '">';
                                echo '<div class="cart-product-info">';
                                echo '<p class="in-cart-naziv">' . $row['naziv'] . '</p>';
                                echo '<p class="in-cart-boja">' . $row['boja'] . '</p>';
                                echo '</div>';

                                echo '<div class="remove-price-container">';
                                echo '<a href="cart.php?delete_product_from_cart=' . $row['idKosarice'] . '" class="remove-button-from-cart">';
                                echo '<i class="fas fa-trash-alt"></i> Ukloni proizvod';
                                echo '</a>';
                                echo '<div class="cart-price">';
                                echo '<p class="cijenaEuri">' . $row['cijenaEuri'] . ' €</p>';
                                echo '<p class="cijenaKune">' . $row['cijenaKune'] . ' kn</p>';
                                echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }
                        echo '<div class="cart-summary">
                                <div class="checkout-section">
                                    <p>Jeste li završili s Vašom kupnjom?</p>
                                    <button type="button" id="openModalButton" class="buy-button">KUPI</button>
                                </div>
                            </div>';
                    } else {
                        echo '<p>Vaša je košarica prazna.</p>';
                    }
                } else {
                    echo '<p>Prijavite se kako biste vidjeli košaricu.</p>';
                }
                ?>
            </div>

            <div id="myModal" class="modal-buy">
                <div class="modal-content-buy">
                    <span class="close custom-close-class">&times;</span>
                    <h2>Unesite podatke za kupnju</h2>
                    <form id="purchaseForm" method="POST">
                        <label class="custom-label-class" for="adresa">Adresa:</label>
                        <input class="custom-input-class" type="text" id="adresa" name="adresa" autocomplete="off" required>
                        <label class="custom-label-class" for="brojMobitela">Broj mobitela:</label>
                        <input class="custom-input-class" type="text" id="brojMobitela" name="brojMobitela" autocomplete="off" required>
                        <label class="custom-label-class" for="iban">IBAN:</label>
                        <input class="custom-input-class" type="text" id="iban" name="iban" required>
                        <button type="button" id="confirmPurchase" class="buy-button">POTVRDI KUPNJU</button>
                    </form>
                </div>
            </div>

        </section>
    </div>
    <script src="js/modal_cart.js"></script>
</body>
</html>