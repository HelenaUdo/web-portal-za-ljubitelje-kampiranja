<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Članci</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php
            include "header.php";
            ?>

            <section id="head">
                <div id="navbar">
                    <a class="active" href="front_page.php">Kamping u Hrvatskoj</a>
                </div>
            </section>

            <section id="articles" class="section-p1">
                <div class="articles-intro">
                    <p class="clanci_first_intro">Dobrodošli na stranicu članaka</p>
                    <p class="clanci_second_intro">Na ovoj stranici možete pročitati razne zanimljive članke o kampiranju, 
                       savjetima za pripremu i mnogo više. Inspirirajte se i saznajte nove informacije 
                       koje će vam pomoći da uživate u vašem kampiranju.</p>
                </div>
                <div class="articles-list">
                <?php

                $sql = "SELECT * FROM clanci";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '<div class="article hidden">';
                        echo '<img src="' . $row['slikaClanka'] . '" alt="' . $row['naslovClanka'] . '" class="article-image">';
                        echo '<div class="article-content">';
                        echo '<h3 class="article-title">' . $row['naslovClanka'] . '</h3>';
                        echo '<p class="article-text">' . $row['tekstClanka'] . '</p>';
                        echo '</div>';
                        echo '</div>';
                    }
                } else {
                    echo "Nema dostupnih članaka.";
                }

                $conn->close();
                ?>
                </div>
            </section>
        </div>
        <?php
        include "footer.php"; 
        ?>
        <script src="js/script_article_show.js"></script>
    </body>
</html>