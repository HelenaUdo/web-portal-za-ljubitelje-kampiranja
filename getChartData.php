<?php
include "base.php";

$sql = "SELECT d.naziv AS naziv_destinacije, SUM(r.brojMjesta) AS ukupan_broj_mjesta
        FROM destinacije d
        LEFT JOIN rezervacije r ON d.idDestinacije = r.IdDestinacije
        GROUP BY d.idDestinacije, d.naziv";

$result = $conn->query($sql);
$dataArray = array();
if ($result && $result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $dataArray[] = array(
            "naziv_destinacije" => $row['naziv_destinacije'],
            "ukupan_broj_mjesta" => intval($row['ukupan_broj_mjesta']),
            "boja" => getRandomColor()
        );
    }
}

$conn->close();

header('Content-Type: application/json');
echo json_encode($dataArray);

function getRandomColor() {
    $letters = '0123456789ABCDEF';
    $color = '#';
    for ($i = 0; $i < 6; $i++) {
        $color .= $letters[rand(0, 15)];
    }
    return $color;
}
?>