<?php
include "base.php";

if(isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];
    
    if(isset($_POST['naziv'], $_POST['cijenaEuri'], $_POST['cijenaKune'], $_POST['opis'], $_POST['podrucje'], $_POST['kolicina'], $_POST['datum'])) {
        $naziv = $_POST['naziv'];
        $cijenaEuri = $_POST['cijenaEuri'];
        $cijenaKune = $_POST['cijenaKune'];
        $opis = $_POST['opis'];
        $podrucje = $_POST['podrucje'];
        $kolicina = $_POST['kolicina'];
        $datum = $_POST['datum'];

        $naziv = mysqli_real_escape_string($conn, $naziv);
        $cijenaEuri = mysqli_real_escape_string($conn, $cijenaEuri);
        $cijenaKune = mysqli_real_escape_string($conn, $cijenaKune);
        $opis = mysqli_real_escape_string($conn, $opis);
        $podrucje = mysqli_real_escape_string($conn, $podrucje);
        $kolicina = mysqli_real_escape_string($conn, $kolicina);
        $datum = mysqli_real_escape_string($conn, $datum);

        $sql = "UPDATE destinacije SET 
                naziv='$naziv', 
                cijenaEuri='$cijenaEuri', 
                cijenaKune='$cijenaKune',
                opis='$opis', 
                podrucje='$podrucje', 
                kolicina='$kolicina', 
                datum='$datum' 
                WHERE idDestinacije='$edit_id'";

        if ($conn->query($sql)) {
            $_SESSION['success_message'] = "Destinacija je uspješno ažurirana";
            header("location: destination_list.php");
            exit();
        } else {
            echo "Error: " . $sql . ": -" . mysqli_error($conn);
        }
    }

    $sql = "SELECT * FROM destinacije WHERE idDestinacije='$edit_id'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $naziv = $row['naziv'];
        $cijenaEuri = $row['cijenaEuri'];
        $cijenaKune = $row['cijenaKune'];
        $opis = $row['opis'];
        $podrucje = $row['podrucje'];
        $kolicina = $row['kolicina'];
        $datum = $row['datum'];
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="Width=device-width, initial-scale=1.0">
    <title>Uredi destinaciju</title>
    <link rel="stylesheet" href="css/style_camp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
</head>
<body>
    <div class="content-container">
        <?php include "header.php"; ?>

        <section id="destinacija_opis" class="section-p1">
            <div class="prviopis">
                <h2>Uredi destinaciju:</h2>
            </div>
        </section>

        <section id="destinacija_unos" class="section-p1">
            <form action="" method="post">
                <input type="text" name="naziv" id="naziv" value="<?php echo $naziv; ?>" autocomplete="off">
                <input type="text" name="cijenaEuri" id="cijenaEuri" value="<?php echo $cijenaEuri; ?>" autocomplete="off">
                <input type="text" name="cijenaKune" id="cijenaKune" value="<?php echo $cijenaKune; ?>" autocomplete="off">
                <textarea name="opis" id="opis" autocomplete="off"><?php echo $opis; ?></textarea>
                <input type="text" name="podrucje" id="podrucje" value="<?php echo $podrucje; ?>" autocomplete="off">
                <input type="text" name="kolicina" id="kolicina" value="<?php echo $kolicina; ?>" autocomplete="off">
                <input type="text" name="datum" id="datum" value="<?php echo $datum; ?>" autocomplete="off">
                <button class="send_normal" id="uredi">Uredi</button>
                <button style='margin-top: 10px;' class="send_normal" id="odustani" onclick="confirmExit()">Odustani</button><br><br>
            </form>
        </section>
    </div>

    <script>
        function confirmExit() {
            var confirmResult = confirm("Jeste li sigurni da želite napustiti stranicu? Promjene neće biti spremljene.");
            if (confirmResult) {
                window.location.href = 'destination_list.php';
            }
        }
    </script>
</body>
</html>