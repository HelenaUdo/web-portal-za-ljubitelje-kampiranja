google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Destinacija');
    data.addColumn('number', 'Ukupan broj mjesta');

    fetchChartDataAndDrawChart(data);
}

function fetchChartDataAndDrawChart(data) {
    fetch('getChartData.php')
    .then(response => response.json())
    .then(dataArray => {
        dataArray.forEach(function(item) {
            data.addRow([item.naziv_destinacije, item.ukupan_broj_mjesta]);
        });

        var options = {
            title: 'UKUPAN BROJ REZERVACIJA PO DESTINACIJI',
            width: 900,
            height: 500,
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    })
    .catch(error => console.error('Greška prilikom dohvaćanja podataka:', error));
}