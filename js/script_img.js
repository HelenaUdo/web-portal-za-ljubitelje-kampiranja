window.addEventListener('load', function () {
    var slikaPrva = document.querySelector('#destinationDetails .slika-container img:first-child');
    var slikaDruga = document.querySelector('#destinationDetails .slika-container img:last-child');
    var slikeContainer = document.querySelector('#destinationDetails .slike-container');

    slikaPrva.style.opacity = 0;
    slikaDruga.style.opacity = 0;

    setTimeout(function () {
        slikaPrva.style.transition = 'opacity 1s ease-in-out, transform 1s ease-in-out';
        slikaPrva.style.opacity = 1;
        slikaPrva.style.transform = 'translateY(0)';
    }, 200);

    setTimeout(function () {
        slikaDruga.style.transition = 'opacity 1s ease-in-out, transform 1s ease-in-out';
        slikaDruga.style.opacity = 1;
        slikaDruga.style.transform = 'translateY(0)';
    }, 600);

    slikeContainer.style.opacity = 0;
    setTimeout(function () {
        slikeContainer.style.transition = 'opacity 1s ease-in-out';
        slikeContainer.style.opacity = 1;
    }, 200);
});
