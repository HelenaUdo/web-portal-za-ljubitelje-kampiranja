var modal = document.getElementById("myModal");
var btn = document.getElementById("openModalButton");
var span = document.getElementsByClassName("close")[0];

btn.onclick = function () {
    modal.style.display = "block";
};

span.onclick = function () {
    modal.style.display = "none";
};

// Obrada potvrde kupnje
var confirmPurchaseButton = document.getElementById("confirmPurchase");

confirmPurchaseButton.onclick = function () {
    var adresa = document.getElementById("adresa").value;
    var brojMobitela = document.getElementById("brojMobitela").value;
    var iban = document.getElementById("iban").value;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "process_purchase.php", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            modal.style.display = "none";
            alert("Kupnja je uspješno obavljena.");
            window.location.reload();
        }
    };

    xhr.send("adresa=" + adresa + "&brojMobitela=" + brojMobitela + "&iban=" + iban);
};