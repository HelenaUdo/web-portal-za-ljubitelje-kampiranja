const articles = document.querySelectorAll('.article');

articles.forEach((article, index) => {
    setTimeout(() => {
        article.classList.remove('hidden');
    }, 700 * index); 
});