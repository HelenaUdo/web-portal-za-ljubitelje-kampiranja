google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawProductChart);

function drawProductChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Proizvod');
    data.addColumn('number', 'Broj kupnji');

    fetchProductChartDataAndDrawChart(data);
}

function fetchProductChartDataAndDrawChart(data) {
    fetch('getChartDataProizvodi.php')
    .then(response => response.json())
    .then(dataArray => {
        dataArray.forEach(function(item) {
            data.addRow([item.naziv_proizvoda, item.broj_kupnji]);
        });

        var options = {
            title: 'UKUPNI BROJ KUPNJI PO PROIZVODU',
            width: 900,
            height: 500,
            colors: ['#f73e9b'],
        };

        var chart = new google.visualization.BarChart(document.getElementById('product_chart_div'));
        chart.draw(data, options);
    })
    .catch(error => console.error('Greška prilikom dohvaćanja podataka:', error));
}