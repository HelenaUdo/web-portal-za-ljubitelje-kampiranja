$(document).ready(function() {
    $('.star').on('click', function () {
        const rating = $(this).data('rating');
        const destinationId = $(this).parent().data('destination-id');
        
        $.ajax({
            type: 'POST',
            url: 'process_rating.php', 
            data: { rating: rating, destinationId: destinationId },
            success: function (response) {
                $('.selected-rating').text(rating);
            }
        });
    });
});