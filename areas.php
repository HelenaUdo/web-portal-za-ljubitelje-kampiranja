<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Pojedinosti odredišta</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php
            include "header.php";
            ?>

            <section id="head">
                <div id="navbar">
                    <a class="active" href="front_page.php">Kamping u Hrvatskoj</a>
                </div>
            </section>

            <section id="mjestaOdabira" class="section-p1 is-sticky">
                <div class="pro-container">
                    <a href="areas.php?podrucje=Slavonija"<?php if (isset($_GET['podrucje']) && $_GET['podrucje'] === 'Slavonija') echo ' class="active"'; ?>>Slavonija</a>
                    <a href="areas.php?podrucje=Zagorje"<?php if (isset($_GET['podrucje']) && $_GET['podrucje'] === 'Zagorje') echo ' class="active"'; ?>>Zagorje</a>
                    <a href="areas.php?podrucje=Istra"<?php if (isset($_GET['podrucje']) && $_GET['podrucje'] === 'Istra') echo ' class="active"'; ?>>Istra</a>
                    <a href="areas.php?podrucje=Dalmacija"<?php if (isset($_GET['podrucje']) && $_GET['podrucje'] === 'Dalmacija') echo ' class="active"'; ?>>Dalmacija</a>
                    <a href="areas.php?podrucje=Lika%20i%20Gorski%20kotar"<?php if (isset($_GET['podrucje']) && $_GET['podrucje'] === 'Lika i Gorski kotar') echo ' class="active"'; ?>>Lika i Gorski kotar</a>
                </div>
            </section>

            <section id="destinacijeFront" class="section-p1">
                <div class="prviopis">
                    <h2>Kamp destinacije:</h2>
                </div>
                <div class="destinacijeNaFront">
                    <?php
                    function displayStarRating($rating) {
                        $fullStar = '<i class="fas fa-star"></i>';
                        $halfStar = '<i class="fas fa-star-half-alt"></i>';
                        $emptyStar = '<i class="far fa-star"></i>';
                        
                        $output = '';
            
                        $roundedRating = round($rating * 2) / 2;
            
                        for ($i = 1; $i <= 5; $i++) {
                            if ($i <= $roundedRating) {
                                $output .= $fullStar;
                            } elseif ($i - 0.5 == $roundedRating) {
                                $output .= $halfStar;
                            } else {
                                $output .= $emptyStar;
                            }
                        }
            
                        echo $output;
                    }
                    if (isset($_GET['podrucje'])) {
                        $selectedPodrucje = $_GET['podrucje'];
                        
                        $sql = "SELECT * FROM destinacije WHERE podrucje = '$selectedPodrucje' ORDER BY idDestinacije ASC";
                        $result = $conn->query($sql);

                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                $sqlOcjene = "SELECT AVG(ocjena) AS prosjecna_ocjena FROM ocjene 
                                              WHERE idDestinacije = '" . $row['idDestinacije'] . "'";
                                $resultOcjene = $conn->query($sqlOcjene);
                
                                if ($resultOcjene->num_rows > 0) {
                                    $rowOcjene = $resultOcjene->fetch_assoc();
                                    $prosjecnaOcjena = $rowOcjene['prosjecna_ocjena'];
                                } else {
                                    $prosjecnaOcjena = 0;
                                }

                                echo '<div class="destinacijaWrapper">';
                                    echo '<div class="kampDestinacija">';
                                        echo '<img src="' . $row['slikaPrva'] . '" alt="' . $row['naziv'] . '">';
                                        echo '<div class="tekst">';
                                            echo '<h3>' . $row['naziv'] . '</h3>';
                                            echo '<p class="podrucjeFront">' . $row['podrucje'] . '</p>';
                                            echo '<p>' . $row['datum'] . '</p>';
                                        echo '</div>';
                                        echo '<div class="ocjena">';
                                            displayStarRating($prosjecnaOcjena);
                                        echo '</div>';
                                        echo '<div class="cijeneGumb">';
                                            echo '<p class="cijenaEuriFront">' . $row['cijenaEuri'] . ' €</p>';
                                            echo '<p class="cijenaKuneFront">' . $row['cijenaKune'] . ' kn</p>';
                                            echo '<a href="destination_details.php?id=' . $row['idDestinacije'] . '">Pojedinosti &gt;</a>';
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            }
                        } else {
                            echo "Nema dostupnih destinacija za odabrano područje.";
                        }
                    } else {
                        echo "Nije odabrano područje.";
                    }
                    ?>
                </div>
            </section>
        </div>
        <?php
        include "footer.php"; 
        ?>
    </body>
</html>