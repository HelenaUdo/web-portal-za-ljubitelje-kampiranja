<?php
session_start();
include "base.php";

if ($_SESSION && isset($_POST['rezerviraj'], $_POST['id_destinacije'], $_POST['broj_mjesta'])) {
    $id_destinacije = $_POST['id_destinacije'];
    $broj_mjesta = intval($_POST['broj_mjesta']);
    $email = $_SESSION['email'];

    $sql_check = "SELECT kolicina FROM destinacije WHERE idDestinacije = '$id_destinacije'";
    $result_check = $conn->query($sql_check);

    if ($result_check && $result_check->num_rows > 0) {
        $row_check = $result_check->fetch_assoc();
        if ($broj_mjesta <= $row_check['kolicina']) {
            $updated_kolicina = $row_check['kolicina'] - $broj_mjesta;
            $sql_update_kolicina = "UPDATE destinacije SET kolicina = '$updated_kolicina' WHERE idDestinacije = '$id_destinacije'";
            if ($conn->query($sql_update_kolicina) === TRUE) {
                $sql_insert = "INSERT INTO rezervacije (email, idDestinacije, brojMjesta) VALUES ('$email', '$id_destinacije', '$broj_mjesta')";
                if ($conn->query($sql_insert) === TRUE) {
                    $sql_destination = "SELECT idDestinacije FROM destinacije WHERE idDestinacije = '$id_destinacije'";
                    $result_destination = $conn->query($sql_destination);

                    if ($result_destination && $result_destination->num_rows > 0) {
                        $row_destination = $result_destination->fetch_assoc();
                        $selectedDestinationID = $row_destination['idDestinacije'];
                        header("Location: destination_details.php?id=$selectedDestinationID&reservation_success=true");
                        exit();
                    } else {
                        echo "Nema dostupnih informacija za odabranu destinaciju.";
                    }
                } else {
                    echo "Greška prilikom rezervacije: " . $conn->error;
                }
            } else {
                echo "Greška prilikom ažuriranja broja mjesta: " . $conn->error;
            }
        } else {
            echo "Nema dovoljno dostupnih mjesta.";
        }
    }
}
?>