<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Prijava</title>
        <link rel="stylesheet" href="css/goin.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div>
            <?php 
            include "base.php";
            $msg='';
            if (isset($_POST['prijavise']) && !empty($_POST['email']) && !empty($_POST['lozinka'])){
                $sql="SELECT * FROM korisnici";
                $q=mysqli_query($conn,$sql);
            
                while ($redak = mysqli_fetch_array($q,MYSQLI_ASSOC)){
                    if(($_POST)['email'] == $redak["email"] && $_POST['lozinka'] == $redak['lozinka']){
                        $_SESSION['prijavljen'] = true;
                        $_SESSION['timeout'] = time();
                        $_SESSION['email'] = $_POST['email'];
                        $_SESSION['ime'] = $redak['ime'];
                        $_SESSION['prezime'] = $redak['prezime'];
                        $_SESSION['uloga'] =$redak['uloga'];
                        if($redak['uloga'] == "admin")
                            header("Location: admin_page.php");
                        else if($redak['uloga'] == "kupac"){
                            header("Location: front_page.php");
                        }
                    }else{
                        $msg = 'Krivo korisničko ime ili lozinka!';
                    }
                }
            }
            ?>
        </div>
        <form action="" method="post">
            <div id="log" class="contain">
              <img src="images_base/logokampa-removebg-preview.png" alt="LogoKampa"><br> 
              <label for="email"><b>E-mail</b></label>
              <input type="text" placeholder="Unesite e-mail" name="email" required>
    
              <label for="lozinka"><b>Lozinka</b></label>
              <input type="password" placeholder="Unesite lozinku" name="lozinka" required>
          
              <button type="submit" class="goinbutton" name="prijavise">Prijava</button>

              <h4><?php echo $msg; ?></h4>
            </div>
        </form>
    </body>
</html>