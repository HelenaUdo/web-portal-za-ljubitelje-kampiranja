<?php
include "base.php";

if(isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];
    
    if(isset($_POST['naziv'], $_POST['kolicinaProizvoda'], $_POST['boja'], $_POST['cijenaEuri'], $_POST['cijenaKune'])){
        $naziv = $_POST['naziv'];
        $kolicinaProizvoda = $_POST['kolicinaProizvoda'];
        $boja = $_POST['boja'];
        $cijenaEuri = $_POST['cijenaEuri'];
        $cijenaKune = $_POST['cijenaKune'];

        $naziv = mysqli_real_escape_string($conn, $naziv);
        $kolicinaProizvoda = mysqli_real_escape_string($conn, $kolicinaProizvoda);
        $boja = mysqli_real_escape_string($conn, $boja);
        $cijenaEuri = mysqli_real_escape_string($conn, $cijenaEuri);
        $cijenaKune = mysqli_real_escape_string($conn, $cijenaKune);

        $sql = "UPDATE proizvodi SET 
                naziv='$naziv', 
                kolicinaProizvoda='$kolicinaProizvoda',
                boja='$boja', 
                cijenaEuri='$cijenaEuri', 
                cijenaKune='$cijenaKune' 
                WHERE IdProizvoda='$edit_id'";

        if ($conn->query($sql)) {
            $_SESSION['success_message'] = "Proizvod je uspješno ažuriran";
            header("location: products_list.php");
            exit();
        } else {
            echo "Error: " . $sql . ": -" . mysqli_error($conn);
        }
    }

    $sql = "SELECT * FROM proizvodi WHERE IdProizvoda='$edit_id'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $naziv = $row['naziv'];
        $kolicinaProizvoda = $row['kolicinaProizvoda'];
        $boja = $row['boja'];
        $cijenaEuri = $row['cijenaEuri'];
        $cijenaKune = $row['cijenaKune'];
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="Width=device-width, initial-scale=1.0">
    <title>Uredi proizvod</title>
    <link rel="stylesheet" href="css/style_camp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
</head>
<body>
    <div class="content-container">
        <?php include "header.php"; ?>

        <section id="destinacija_opis" class="section-p1">
            <div class="prviopis">
                <h2>Uredi proizvod:</h2>
            </div>
        </section>

        <section id="destinacija_unos" class="section-p1">
            <form action="" method="post">
                <input type="text" name="naziv" id="naziv" value="<?php echo $naziv; ?>" autocomplete="off">
                <input type="text" name="kolicinaProizvoda" id="kolicinaProizvoda" value="<?php echo $kolicinaProizvoda; ?>" autocomplete="off">
                <input type="text" name="boja" id="boja" value="<?php echo $boja; ?>" autocomplete="off">
                <input type="text" name="cijenaEuri" id="cijenaEuri" value="<?php echo $cijenaEuri; ?>" autocomplete="off">
                <input type="text" name="cijenaKune" id="cijenaKune" value="<?php echo $cijenaKune; ?>" autocomplete="off">
                <button class="send_normal" id="uredi">Uredi</button>
                <button style='margin-top: 10px;' class="send_normal" id="odustani" onclick="confirmExit()">Odustani</button><br><br>
            </form>
        </section>
    </div>

    <script>
        function confirmExit() {
            var confirmResult = confirm("Jeste li sigurni da želite napustiti stranicu? Promjene neće biti spremljene.");
            if (confirmResult) {
                window.location.href = 'products_list.php';
            }
        }
    </script>
</body>
</html>