<?php
include "base.php";
session_start();

function hasItemsInCart($email, $conn) {
    $sqlCheckCart = "SELECT COUNT(*) as count FROM kosarica WHERE email = '$email'";
    $resultCheckCart = mysqli_query($conn, $sqlCheckCart);
    $rowCheckCart = mysqli_fetch_assoc($resultCheckCart);
    $cartItemCount = $rowCheckCart['count'];
    return ($cartItemCount > 0);
}

if ($_SESSION == NULL) {
?>
<section id="firsthead">
    <div id="firstnavbar">
        <div class="logo">
            <img src="images_base/logokampa-removebg-preview.png" alt="LogoKampa" class="logo-imag">
            Web portal za ljubitelje kampiranja
        </div>
        <div class="basket-profile">
            <a href="cart.php" class="basket">
                <i class="icon fas fa-shopping-basket"></i>
            </a>
            <a href="article.php" class="articles">
                <i class="icon fas fa-newspaper"></i>
            </a>
            <div class="profile">
                <i class="icon fas fa-user"></i>
                Profil
                <div class="dropdown-content">
                    <a href="login.php">Prijava</a>
                    <a href="register.php">Registracija</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php  
} else {
    $email = $_SESSION['email']; 
    $sql = "SELECT ime, prezime, uloga FROM korisnici WHERE email = '$email'";
    $result = mysqli_query($conn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $ime = $row['ime'];
        $prezime = $row['prezime'];
        $uloga = $row['uloga'];

        $hasItems = hasItemsInCart($email, $conn);

        $basketClass = $hasItems ? 'fill-basket' : 'emp-basket';
?>

<section id="firsthead">
    <div id="firstnavbar">
        <div class="logo">
            <img src="images_base/logokampa-removebg-preview.png" alt="LogoKampa" class="logo-imag">
            Web portal za ljubitelje kampiranja
        </div>
        <div class="basket-profile">
            <?php if ($uloga === 'kupac') { ?>
                <a href="cart.php" class="basket <?php echo $basketClass; ?>">
                    <i class="icon fas fa-shopping-basket"></i>
                </a>
                <a href="article.php" class="articles">
                    <i class="icon fas fa-newspaper"></i>
                </a>
            <?php } ?>
            <div class="profile">
                <i class="icon fas fa-user"></i>
                <?php echo $ime . ' ' . $prezime; ?>
                <div class="dropdown-content">
                    <a href="signout.php">Odjava</a>
                    <?php
                    if ($uloga === 'kupac') {
                        echo '<a href="my_camp.php">Moj kamp</a>';
                    } elseif ($uloga === 'admin') {
                        echo '<a href="notice.php">Obavijesti</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    } else {
        // redirect to the login page.
        header("Location: login.php");
        exit;
    }
}
?>