<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Pojedinosti odredišta</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php include "header.php"; ?>

            <section id="destinationDetails" class="section-p1">
                <?php
                if (isset($_GET['id'])) {
                    $selectedDestinationID = $_GET['id'];

                    $sql = "SELECT * FROM destinacije WHERE idDestinacije = '$selectedDestinationID'";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            echo '<h2>' . $row['naziv'] . '</h2>';
                            echo '<p class="podrucjeFront">' . $row['podrucje'] . '</p>';
                            echo '<div class="slika-container">';
                                echo '<img src="' . $row['slikaPrva'] . '" alt="' . $row['naziv'] . '">';
                                echo '<img src="' . $row['slikaDruga'] . '" alt="' . $row['naziv'] . '">';
                            echo '</div>';
                            echo '<div class="destinationDetailsWrapper">'; 
                                echo '<p class="kolikoFront"> Mjesta: ' . $row['kolicina'] . '</p>';
                                echo '<p class="mojDatumFront">' . $row['datum'] . '</p>';
                                echo '<p class="cijenaEuriFront">' . $row['cijenaEuri'] . ' €</p>';
                                echo '<p class="cijenaKuneFront">' . $row['cijenaKune'] . ' kn</p>';
                                echo '<p>' . $row['opis'] . '</p>';
                            echo '</div>';
                            if(isset($_SESSION['email'])) {
                                echo '<div class="reservation-form">';
                                    echo '<form action="process_reservation.php" method="post">';
                                        echo '<label for="broj_mjesta">Odaberite broj mjesta:</label>';
                                        echo '<input type="number" name="broj_mjesta" min="1" max="' . $row['kolicina'] . '" required>';
                                        echo '<input type="hidden" name="id_destinacije" value="' . $selectedDestinationID . '">';
                                        echo '<button type="submit" name="rezerviraj">Rezerviraj</button>';
                                    echo '</form>';
                                echo '</div>';
                            }
                        }
                    } else {
                        echo "Nema dostupnih informacija za odabranu destinaciju.";
                    }
                } else {
                    echo "Nije odabrana destinacija za prikaz.";
                }
                ?>

                <div class="comments-list">
                    <h3 style='margin-top: 60px'>Komentari:</h3>
                    <?php
                    $sql = "SELECT r.komentar, k.ime FROM recenzije r INNER JOIN korisnici k ON r.email = k.email WHERE r.idDestinacije = '$selectedDestinationID'";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            echo '<div class="comment">';
                            echo '<p class="comment-user">' . $row['ime'] . '</p>';
                            echo '<p class="comment-text">' . $row['komentar'] . '</p>';
                            echo '</div>';
                        }
                    } else {
                        echo '<p>Nema dostupnih komentara.</p>';
                    }
                    ?>
                </div>

                <div class="comments-section">
                    <h3>Ostavite svoj komentar:</h3>
                    <?php
                    if (isset($_SESSION['email'])) {
                        echo '<form action="process_comment.php" method="post">';
                        echo '<input type="hidden" name="id_destinacije" value="' . $selectedDestinationID . '">';
                        echo '<textarea name="komentar" placeholder="Unesite svoj komentar" required></textarea>';
                        echo '<button type="submit" name="ostavi_komentar">Unesi</button>';
                        echo '</form>';
                    } else {
                        echo '<p>Za ostavljanje komentara morate biti ulogirani.</p>';
                    }
                    ?>
                </div>

                <div class="rating">
                    <h3>Ocijenite destinaciju:</h3>
                    <?php
                    if (isset($_SESSION['email'])) {
                        echo '<div class="stars" data-destination-id="' . $selectedDestinationID . '">';
                        echo '<i class="far fa-star star" data-rating="1"></i>';
                        echo '<i class="far fa-star star" data-rating="2"></i>';
                        echo '<i class="far fa-star star" data-rating="3"></i>';
                        echo '<i class="far fa-star star" data-rating="4"></i>';
                        echo '<i class="far fa-star star" data-rating="5"></i>';
                        echo '</div>';
                        echo '<p class="user-rating">Vaša ocjena: <span class="selected-rating">Nema ocjene</span></p>';
                    } else {
                        echo '<p>Morate biti ulogirani da biste mogli ocijeniti destinaciju.</p>';
                    }
                    ?>
                </div>

                <div class="back-button">
                    <a href="javascript:history.back()" class="butn btn-secondary">&lt; Natrag na pretragu</a>
                </div>
            </section> 
        </div>
        <?php
        include "footer.php"; 
        ?>
        <script src="js/script_img.js"></script>
        <script src="js/rating.js"></script>
    </body>
</html>