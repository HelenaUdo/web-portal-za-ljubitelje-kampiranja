<div class="footer-green">
    <div class="footer-content">
        <div class="footer-top">
            <div class="footer-left">
                <img src="images_base/logokampa-removebg-preview.png" alt="LogoKampa" class="logo-imag">
                <p>Web portal za ljubitelje kampiranja</p>
            </div>
            <div class="footer-right">
                <a href="front_page.php">Naslovna</a><br>
                <a class="artFoot" href="article.php">Članci</a><br>
                <a href="store.php">Trgovina</a>
            </div>
        </div>
        <div class="footer-section">
            <div class="footer-left">
                <p>Kneza Trpimira 2B, HR-31000 Osijek | Cara Hadrijana 10b, HR-31000 Osijek</p>
            </div>
            <div class="footer-right">
                <p class="my_add">Tel: +385 (0) 31 224-600 | Fax: +385 (0) 31 224-605</p>
                <p class="my_mail">E-mail: udovicichelena@gmail.com</p>
            </div>
        </div>
    </div>
</div>
