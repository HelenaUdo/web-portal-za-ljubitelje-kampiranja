<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Camp</title>
    <link rel="stylesheet" href="css/style_camp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
</head>
<body>
    <div class="content-container">
        <?php include "header.php"; ?>
        
        <section id="myCamp" class="section-p1">
            <div class="header_container">
                <div class="back-button">
                    <a href="javascript:history.back()" class="butn btn-secondary">&lt;</a>
                </div>
                <h2>Moji rezervirani kampovi</h2>
            </div>
            <div class="camp-list">
                <?php

                if ($_SESSION) {
                    $email = $_SESSION['email'];
                    $sql = "SELECT r.*, d.naziv, d.podrucje, d.slikaPrva, d.datum, d.cijenaEuri, d.cijenaKune FROM rezervacije r
                            INNER JOIN destinacije d ON r.idDestinacije = d.idDestinacije
                            WHERE r.email = '$email'";
                    $result = mysqli_query($conn, $sql);

                    if ($result && mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<div class="camp-item-container">';
                            echo '<div class="camp-item">';
                                echo '<img src="' . $row['slikaPrva'] . '" alt="' . $row['naziv'] . '">';
                                echo '<h3>' . $row['naziv'] . '</h3>';
                                echo '<p class="podrucjeFront">' . $row['podrucje'] . '</p>';
                                echo '<p>Datum: ' . $row['datum'] . '</p>';
                                echo '<p>Cijena: ' . $row['cijenaEuri'] . ' € / ' . $row['cijenaKune'] . ' kn</p>';
                                echo '<p>Rezervirano mjesta: ' . $row['brojMjesta'] . '</p>';
                                echo '<a href="my_camp.php?delete_reservation=' . $row['idRezervacije'] . '" class="delete-link"><i class="fas fa-trash-alt"></i> Izbriši rezervaciju</a>';
                            echo '</div>';
                            echo '</div>';
                        }
                    } else {
                        echo '<p>Nema rezervacija.</p>';
                    }
                } else {
                    echo '<p>Molimo prijavite se kako biste vidjeli svoje rezervacije.</p>';
                }

                // Brisanje rezervacije
                if ($_SESSION && isset($_GET['delete_reservation'])) {
                    $reservationID = $_GET['delete_reservation'];
                    $sql_reservation_info = "SELECT idDestinacije, brojMjesta FROM rezervacije WHERE idRezervacije = '$reservationID'";
                    $result_reservation_info = mysqli_query($conn, $sql_reservation_info);

                    if ($result_reservation_info && mysqli_num_rows($result_reservation_info) > 0) {
                        $row_reservation_info = mysqli_fetch_assoc($result_reservation_info);
                        $id_destinacije = $row_reservation_info['idDestinacije'];
                        $broj_mjesta_izbrisane_rezervacije = intval($row_reservation_info['brojMjesta']);

                        $sql_update_kolicina_after_delete = "UPDATE destinacije SET kolicina = kolicina + $broj_mjesta_izbrisane_rezervacije WHERE idDestinacije = '$id_destinacije'";
                        if (mysqli_query($conn, $sql_update_kolicina_after_delete)) {
                            
                            $sql_delete = "DELETE FROM rezervacije WHERE idRezervacije = '$reservationID'";
                            if (mysqli_query($conn, $sql_delete)) {
                                header("Location: my_camp.php");
                                exit();
                            } else {
                                echo '<p class="error">Greška prilikom brisanja rezervacije: ' . mysqli_error($conn) . '</p>';
                            }
                        } else {
                            echo '<p class="error">Greška prilikom ažuriranja broja mjesta nakon brisanja rezervacije: ' . mysqli_error($conn) . '</p>';
                        }
                    } else {
                        echo '<p class="error">Nema informacija o odabranoj rezervaciji.</p>';
                    }
                }
                ?>
            </div>
        </section>
    </div>
</body>
</html>