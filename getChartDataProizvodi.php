<?php
include "base.php";

$sql = "SELECT p.naziv AS naziv_proizvoda, COUNT(k.IdProizvoda) AS broj_kupnji
        FROM proizvodi p
        LEFT JOIN kupljeniproizvodi k ON p.IdProizvoda = k.IdProizvoda
        GROUP BY p.IdProizvoda, p.naziv";
        
$result = $conn->query($sql);
$dataArray = array();
if ($result && $result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $dataArray[] = array(
            "naziv_proizvoda" => $row['naziv_proizvoda'],
            "broj_kupnji" => intval($row['broj_kupnji']),
            "boja" => getRandomColor()
        );
    }
}

$conn->close();

header('Content-Type: application/json');
echo json_encode($dataArray);

function getRandomColor() {
    $letters = '0123456789ABCDEF';
    $color = '#';
    for ($i = 0; $i < 6; $i++) {
        $color .= $letters[rand(0, 15)];
    }
    return $color;
}
?>