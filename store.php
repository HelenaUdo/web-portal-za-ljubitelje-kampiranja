<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WebCamp Trgovina</title>
    <link rel="stylesheet" href="css/style_camp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
</head>
<body>
    <div class="content-container">
        <?php include "header.php"; ?>
        <section id="head">
            <div id="navbar">
                <a href="front_page.php">Kamping u Hrvatskoj</a>
            </div>
        </section>

        <section id="store" class="section-p1">
        <div class="store-intro">
            <p class="store_first_intro">Dobrodošli u naš Webshop</p>
            <p class="store_second_intro">Ovdje ćete pronaći širok izbor proizvoda koji će vaše kampiranje učiniti još boljim i udobnijim. Bez obzira jeste li iskusni avanturist ili tek počinjete svoje kamperske pustolovine, naša ponuda obuhvaća sve što vam je potrebno za nezaboravno iskustvo u prirodi.</p>
            <p class="store_second_intro">Budite spremni za sve izazove koje priroda može donijeti uz proizvode koji su pouzdani i prilagođeni vašim potrebama. Kliknite i istražite našu ponudu kako biste pronašli sve što vam je potrebno za nezaboravno kampiranje!</p>
        </div>
            <div class="store-grid">
                <?php

                $sql = "SELECT * FROM proizvodi";
                $result = mysqli_query($conn, $sql);

                if ($result && mysqli_num_rows($result) > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo '<div class="product-item-container">';
                        echo '<div class="product-item">';
                        echo '<a href="product_details.php?id=' . $row['IdProizvoda'] . '"><img class="slikaStore" src="' . $row['slikaProizvoda'] . '" alt="' . $row['naziv'] . '"></a>';
                            echo '<p class="nazivStore">' . $row['naziv'] . '</p>';
                            echo '<p class="euriStore">' . $row['cijenaEuri'] . ' €</p>';
                            echo '<p class="kuneStore">' . $row['cijenaKune'] . ' kn</p>';
                        echo '</div>';
                        echo '</div>';
                    }
                } else {
                    echo '<p>Nema dostupnih proizvoda.</p>';
                }
                ?>
            </div>
        </section>
    </div>
    <?php 
    include "footer.php"; 
    ?>
</body>
</html>