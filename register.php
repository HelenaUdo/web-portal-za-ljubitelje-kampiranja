<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Registracija</title>
        <link rel="stylesheet" href="css/goin.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <?php
        require_once "base.php";

        if (!empty($_POST['email'])) {
            unset($error);

            $sql = 'SELECT * FROM korisnici WHERE email = "'.$_POST['email'].'"';
            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                $error = "Email zauzet!<br>";
            } else {
                $ime = $_POST['ime'];
                $prezime = $_POST['prezime'];
                $email = $_POST['email'];
                $lozinka = $_POST['lozinka'];

                $hashedPassword = password_hash($lozinka, PASSWORD_DEFAULT);

                $sql = "INSERT INTO korisnici (ime,prezime,email,lozinka,uloga) 
                        VALUES('$ime','$prezime','$email','$hashedPassword','kupac')";
                if (mysqli_query($conn, $sql)) {
                    // Automatska prijava nakon registracije
                    $_SESSION['prijavljen'] = true;
                    $_SESSION['timeout'] = time();
                    $_SESSION['email'] = $email;
                    $_SESSION['ime'] = $ime;
                    $_SESSION['prezime'] = $prezime;
                    $_SESSION['uloga'] = 'kupac';
                    
                    header("Location: front_page.php");
                    exit();
                } else {
                    echo "Error: " . $sql . ": -" . mysqli_error($conn);
                }
                mysqli_close($conn);
            }
        }
    ?>

    <body>
        <form action="" method="post">
            <div id="registr" class="contain">
              <img src="images_base/logokampa-removebg-preview.png" alt="LogoKampa"><br>
              <p>Ispunite ovu formu za stvaranje svog korisničkog računa.</p>
        
              <label for="ime"><b>Ime</b></label>
              <input type="text" name="ime" id="ime" placeholder= "Unesite ime" autocomplete="off"><br>
              
              <label for="prezime"><b>Prezime</b></label>
              <input type="text" name="prezime" id="prezime" placeholder= "Unesite prezime" autocomplete="off"><br>
              
              <label for="email"><b>E-mail</b></label>
              <input type="text" name="email" id="email" placeholder= "Unesite e-mail" autocomplete="off"><br>
              
              <label for="lozinka"><b>Lozinka</b></label>
              <input type="password" name="lozinka" id="lozinka" placeholder= "Unesite lozinku"><br>
          
              <button type="submit" class="goinbutton" onclick="provjeri()">Registriraj</button>
            </div>
            <div class="container login">
              <p>Već imate svoj račun? <a href="login.php">Prijava</a>.</p>
            </div>
        </form>
        

        <script>
         function provjeri() {
            let ime = document.getElementById("ime").value;
            let imeL = ime.length;
            let prezime = document.getElementById("prezime").value;
            let prezimeL = prezime.length;
            let lozinka = document.getElementById("lozinka").value;
            let lozinkaL = lozinka.length;
            let emailad = document.getElementById("email").value;
            let pozicijaA = emailad.indexOf("@");
            let pozicijatocke = emailad.lastIndexOf(".");
            let emaillen = emailad.length - 1;
            if (imeL == "" || prezimeL == "" || lozinkaL == "") {
                alert("Nisu unešeni svi potrebni podaci!")
                return false;
            }else if (pozicijaA < 2 || (pozicijatocke - pozicijaA) < 2 || (emaillen - pozicijatocke) < 2) {
                alert("Email format nije valjan.")
                return false;
            }
        }
    </script>
    </body>
</html> 
