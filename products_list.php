<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Unos proizvoda</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php
            include "header.php";
            ?>

            <?php 
            require_once "base.php";

            if(isset($_POST['naziv'], $_POST['kolicinaProizvoda'], $_POST['boja'], $_POST['cijenaEuri'], $_POST['cijenaKune'])){
                unset($error);
  
                $naziv = $_POST['naziv'];
                $kolicinaProizvoda = $_POST['kolicinaProizvoda'];
                $boja = $_POST['boja'];
                $cijenaEuri = $_POST['cijenaEuri'];
                $cijenaKune = $_POST['cijenaKune'];
                $targetDir = "images/product_img/";
                $slikaProizvoda = $targetDir . basename($_FILES["slikaProizvoda"]["name"]);
                                
                if (move_uploaded_file($_FILES["slikaProizvoda"]["tmp_name"], $slikaProizvoda)) {
                    $naziv = mysqli_real_escape_string($conn, $naziv);
                    $kolicinaProizvoda = mysqli_real_escape_string($conn, $kolicinaProizvoda);
                    $boja = mysqli_real_escape_string($conn, $boja);
                    $cijenaEuri = mysqli_real_escape_string($conn, $cijenaEuri);
                    $cijenaKune = mysqli_real_escape_string($conn, $cijenaKune);

                    $sql = "INSERT INTO proizvodi (naziv, kolicinaProizvoda, boja, cijenaEuri, cijenaKune, slikaProizvoda) 
                            VALUES ('$naziv', '$kolicinaProizvoda', '$boja', '$cijenaEuri', '$cijenaKune', '$slikaProizvoda')";
                    if ($conn->query($sql)){
                        $_SESSION['success_message'] = "Vaši su podaci uspješno poslani";
                        header("location: products_list.php");
                        exit();
                    }
                    else {
                        echo "Error: " . $sql . ": -" . mysqli_error($conn);
                    }
                } else {
                    echo "Error uploading images.";
                }
                mysqli_close($conn);
            }
            ?>

            <section id="destinacija_opis" class="section-p1">
                <div class="prviopis">
                    <h2>Unesite novi proizvod:</h2>
                </div>
            </section>

            <section id="destinacija_unos" class="section-p1">
                <?php
                if (isset($_SESSION['success_message'])) {
                    echo "<h3>" . $_SESSION['success_message'] . "</h3>";
                    unset($_SESSION['success_message']);
                }
                ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <span>UNESITE PODATKE</span>
                    <input type="text" name="naziv" id="naziv" placeholder="Naziv proizvoda" autocomplete="off">
                    <input type="text" name="kolicinaProizvoda" id="kolicinaProizvoda" placeholder="Količina proizvoda" autocomplete="off">
                    <input type="text" name="boja" id="boja" placeholder="Boja proizvoda" autocomplete="off">
                    <input type="text" name="cijenaEuri" id="cijenaEuri" placeholder="Cijena u eurima" autocomplete="off">
                    <input type="text" name="cijenaKune" id="cijenaKune" placeholder="Cijena u kunama" autocomplete="off">
                    <label for="slikaProizvoda">Slika proizvoda:</label>
                    <input type="file" name="slikaProizvoda" id="slikaProizvoda">  
                    <button class="send_normal" id="otkupise">Dodaj</button><br><br>
                </form>
            </section>

            <section id="destinacija_opis" class="section-p1">
                <div class="prviopis">
                    <h2>Popis proizvoda:</h2>
                </div>
            </section>

            <?php
            // ispis
            $sql = "SELECT * FROM proizvodi";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                echo "<table><tr><th> PROIZVOD </th><th> SLIKA </th><th> KOLIČINA </th><th> BOJA </th><th> CIJENA U EURIMA </th><th> CIJENA U KUNAMA </th></tr>";            
                
                while ($row = $result->fetch_assoc()) {
                    echo  "<tr> ";
                    echo "<td>" .$row['naziv']. "</td>
                        <td><img src='" .$row['slikaProizvoda']. "' alt='" .$row['naziv']. "' width='150'></td>
                        <td>" .$row['kolicinaProizvoda']. "</td>
                        <td>" .$row['boja']. "</td>
                        <td>" .$row['cijenaEuri']. " €</td>
                        <td>" .$row['cijenaKune']. " kn</td>
                        <td style='text-align: center; vertical-align: middle;'>
                            <a href='delete_product.php?delete_id=".$row['IdProizvoda']."'><i class='fa fa-trash'></i></a>
                            <a style='margin-left: 10px; margin-right: -5px;' href='edit_product.php?edit_id=".$row['IdProizvoda']."'><i class='fa fa-edit'></i></a>
                        </td>";
                    echo  "</tr> ";
                }
                echo "</table>";
            } else {
                echo "0 results";
            }

            $conn->close();
            ?>
        </div>
    </body>
</html>