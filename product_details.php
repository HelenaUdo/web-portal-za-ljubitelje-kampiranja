<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>WebShopDetails</title>
        <link rel="stylesheet" href="css/style_camp.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="content-container">
            <?php include "header.php"; ?>
            <section id="head">
                <div id="navbar">
                    <a href="front_page.php">Kamping u Hrvatskoj</a>
                </div>
            </section> 

            <?php
                if (isset($_GET['id'])) {
                    $productId = $_GET['id'];
                    $sql = "SELECT * FROM proizvodi WHERE IdProizvoda = $productId";
                    $result = mysqli_query($conn, $sql);

                    if ($result && mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<div class="product-details">';
                            echo '<div class="left-column">';
                            echo '<img class="slikaDetails" src="' . $row['slikaProizvoda'] . '" alt="' . $row['naziv'] . '">';
                            echo '</div>';

                            echo '<div class="right-column">';
                            echo '<p class="nazivStoreDet">' . $row['naziv'] . '</p>';
                            echo '<p class="bojaStoreDet">' . $row['boja'] . '</p>';
                            echo '<p class="euriStoreDet">' . $row['cijenaEuri'] . ' €</p>';
                            echo '<p class="kuneStoreDet">' . $row['cijenaKune'] . ' kn</p>';  
                                                    
                            if (isset($_SESSION['email']) && $row['kolicinaProizvoda'] > 0) {
                                echo '<form action="add_to_cart.php" method="POST">';
                                echo '<input type="hidden" name="productId" value="' . $productId . '">';
                                echo '<button type="submit" class="add-to-cart-btn">Dodaj u košaricu</button>';
                                echo '</form>';
                            } elseif (!isset($_SESSION['email'])) {   
                                echo '<p>Prijavite se kako biste dodali proizvod u košaricu.</p>';
                            } elseif ($row['kolicinaProizvoda'] <= 0) {   
                                echo '<a href="#" class="add-to-cart-none">Dodaj u košaricu</a>';
                            }         
                                          
                            echo '</div>';

                            echo '</div>';
                        }
                    } else {
                        echo '<p>Nema dostupnih proizvoda.</p>';
                    }
                } else {
                    echo '<p>Nije odabran proizvod za prikaz.</p>';
                }
            ?>
        </div>

        <?php 
        include "footer.php"; 
        ?>

        <script>
            function addToCart(productId) {
                window.location.href = 'add_to_cart.php?productId=' + productId;
            }
        </script>
    </body>
</html>