<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="Width=device-width, initial-scale=1.0">
    <title>Unos članaka</title>
    <link rel="stylesheet" href="css/style_camp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,200;1,300&display=swap" rel="stylesheet">
</head>
<body>
    <div class="content-container">
        <?php
            include "header.php";
        ?>

        <?php
        require_once "base.php";

        if(isset($_POST['naslovClanka'], $_POST['tekstClanka'])) {
            $naslovClanka = $_POST['naslovClanka'];
            $tekstClanka = $_POST['tekstClanka'];

            $targetDir = "images/article_img/";
            $slikaClanka = $targetDir . basename($_FILES["slikaClanka"]["name"]);

            if (move_uploaded_file($_FILES["slikaClanka"]["tmp_name"], $slikaClanka)) {
                $naslovClanka = mysqli_real_escape_string($conn, $naslovClanka);
                $tekstClanka = mysqli_real_escape_string($conn, $tekstClanka);

                $sql = "INSERT INTO clanci (naslovClanka, tekstClanka, slikaClanka) 
                        VALUES ('$naslovClanka', '$tekstClanka', '$slikaClanka')";

                if ($conn->query($sql)) {
                    $_SESSION['success_message'] = "Vaš članak je uspješno dodan";
                    header("location: articles_list.php");
                    exit();
                } else {
                    echo "Error: " . $sql . ": -" . mysqli_error($conn);
                }
            } else {
                echo "Error uploading images.";
            }
            mysqli_close($conn);
        }
        ?>

        <section id="destinacija_opis" class="section-p1">
                <div class="prviopis">
                    <h2>Unesite novi članak:</h2>
                </div>
        </section>

        <section id="destinacija_unos" class="section-p1">
            <?php
            if (isset($_SESSION['success_message'])) {
                echo "<h3>" . $_SESSION['success_message'] . "</h3>";
                unset($_SESSION['success_message']);
            }
            ?>
            <form action="" method="post" enctype="multipart/form-data">
                <span>UNESITE PODATKE</span>
                <input type="text" name="naslovClanka" id="naslovClanka" placeholder="Naslov članka" autocomplete="off">
                <textarea name="tekstClanka" id="tekstClanka" placeholder="Tekst članka" autocomplete="off"></textarea>
                <label for="slikaClanka">Slika članka:</label>
                <input type="file" name="slikaClanka" id="slikaClanka">
                <button class="send_normal" id="otkupise">Dodaj</button><br><br>
            </form>
        </section>

        <section id="destinacija_opis" class="section-p1">
                <div class="prviopis">
                    <h2>Popis članaka:</h2>
                </div>
        </section>
        
        <?php
            // ispis
            $sql = "SELECT * FROM clanci";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                echo "<table><tr><th> NASLOV </th><th> SLIKA </th><th> TEKST </th></tr>";            
                
                while ($row = $result->fetch_assoc()) {
                    echo  "<tr> ";
                    echo "<td>" .$row['naslovClanka']. "</td>
                        <td><img src='" .$row['slikaClanka']. "' alt='" .$row['naslovClanka']. "' width='150'></td>
                        <td class='opis-column'>" .$row['tekstClanka']. "</td>
                        <td style='text-align: center; vertical-align: middle;'>
                            <a href='delete_article.php?delete_id=".$row['idClanka']."'><i class='fa fa-trash'></i></a>
                            <a style='margin-left: 10px; margin-right: -5px;' href='edit_article.php?edit_id=".$row['idClanka']."'><i class='fa fa-edit'></i></a>
                        </td>";
                    echo  "</tr> ";
                }
                echo "</table>";
            } else {
                echo "0 results";
            }

            $conn->close();
        ?>
    </div>
</body>
</html>